package com.example.nzxt.chatapp2firebase;

import android.net.Uri;

import java.util.Date;

/**
 * Created by NZXT on 5-10-2017.
 */

public class ChatMessage {

    private String messageText;
    private String messageUser;
    private String messageName;
    private String messageTime;
    private String imageURL;
    private String password;
    private String rotation;
    private String isOnline;
    private String readMessage;
    private String showRedDot;
    private int notificationID;

    public int getNotificationID() {
        return notificationID;
    }

    public void setNotificationID(int notificationID) {
        this.notificationID = notificationID;
    }

    public String getReadMessage() {
        return readMessage;
    }

    public String getShowRedDot() {
        return showRedDot;
    }

    public void setShowRedDot(String showRedDot) {
        this.showRedDot = showRedDot;
    }

    public ChatMessage() {
    }


    public ChatMessage(String messageText, String messageUser, String messageName) {
        this.messageText = messageText;
        this.messageUser = messageUser;
        this.messageName = messageName;


       // messageTime = new Date().getTime();
    }

    public ChatMessage(String messageText, String messageUser, String messageName, String imageURL){
        this.messageText = messageText;
        this.messageUser = messageUser;
        this.messageName = messageName;
        this.imageURL = imageURL;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getMessageUser() {
        return messageUser;
    }

    public void setMessageUser(String messageUser) {
        this.messageUser = messageUser;
    }

    public String getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(String messageTime) {
        this.messageTime = messageTime;
    }

    public String getMessageName() {
        return messageName;
    }

    public void setMessageName(String messageName) {
        this.messageName = messageName;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String isOnline() {
        return isOnline;
    }

    public void setOnline(String online) {
        isOnline = online;
    }

    public String getRotation() {
        return rotation;
    }

    public void setRotation(String rotation) {
        this.rotation = rotation;
    }

    public String isReadMessage() {
        return readMessage;
    }

    public void setReadMessage(String readMessage) {
        this.readMessage = readMessage;
    }

}
