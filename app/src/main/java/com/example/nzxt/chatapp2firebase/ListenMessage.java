package com.example.nzxt.chatapp2firebase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.firebase.client.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import io.paperdb.Paper;

public class ListenMessage extends Service implements ChildEventListener {

    private FirebaseDatabase db;
    private DatabaseReference messages;
    private DatabaseReference users;
    private List<ChatMessage> listMessages;
    List<String> stringData = new ArrayList<>();
    int i = 0;
    boolean blnTest = true;
    StringBuilder stringBuilder = new StringBuilder();
    int counter = UserDetails.counter;
    boolean sendNotification = false;
    private int notificId;
    private Firebase ref;

    public ListenMessage() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        listMessages = new ArrayList<>();
        db = FirebaseDatabase.getInstance();

        messages = db.getReference("messages");
        users = db.getReference("users");

        Paper.init(getBaseContext());

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        messages.addChildEventListener(this);
        UserDetails.serviceStartID = startId;
        return super.onStartCommand(intent, flags, startId);
        //return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        // Trigger hier
      /*  Request request = dataSnapshot.getValue(Request.class);
        if(request.getStatus().equals("0")){
            showNotification(dataSnapshot.getKey(), request);
        } */

        final String data = dataSnapshot.getKey().toString();

        String S = data;


        UserDetails.counter = UserDetails.counter + 1;
        int temp = S.indexOf('_');
        temp = temp + 1;
        final String result = S.substring(temp);
        stringData.add(result);
        //String user = Paper.book().read("paperUser");
        if (UserDetails.counter == 2) {

            loadUsers();

            UserDetails.counter = 0;
            UserDetails.isSender = false;

            stringData.clear();


        }
    }

    private void showNotification() {
        Intent intent = new Intent(getBaseContext(), Users.class);
        PendingIntent contentIntent = PendingIntent.getActivity(getBaseContext(), 0, intent, 0);

        // NotificationCompat.Builder builder = new NotificationCompat.Builder(getBaseContext());

        Notification not = new NotificationCompat.Builder(getBaseContext())

                // .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setTicker("ChatAppGeert")
                .setContentInfo("Nieuw bericht")
                .setContentText("U hebt een nieuw bericht , klik to open!!")
                .setSmallIcon(R.drawable.chaticon)
                .setContentIntent(contentIntent)
                .build();


        //if you want to many notification show, you need give unique ID for each notification
        notificId = new Random().nextInt(9999 - 1) + 1;
        not.flags |= Notification.FLAG_AUTO_CANCEL;
        UserDetails.notificationManager.notify(0, not);

    }

    public void stopServiceMessaging() {
        messages.removeEventListener(this);
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {


        final String data = dataSnapshot.getKey().toString();

        String S = data;


        UserDetails.counter = UserDetails.counter + 1;
        int temp = S.indexOf('_');
        temp = temp + 1;
        final String result = S.substring(temp);
        stringData.add(result);
        //String user = Paper.book().read("paperUser");
        if (UserDetails.counter == 2) {

            loadUsers();

            UserDetails.counter = 0;
            UserDetails.isSender = false;

            stringData.clear();


        }


    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }

    private void loadUsers() {

        String url = "https://gsonvolleydemo.firebaseio.com/users.json";

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                doOnSuccess(s);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("" + volleyError);
            }
        });

        RequestQueue rQueue = Volley.newRequestQueue(getBaseContext());
        rQueue.add(request);
    }

    public void doOnSuccess(String s) {
        try {
            JSONObject obj = new JSONObject(s);

            JSONObject objj = obj.getJSONObject(UserDetails.username);

            String readMessage = (String) objj.get("readMessage");

            if (!Boolean.parseBoolean(readMessage)) {
                users.child(UserDetails.username).child("readMessage").setValue("true");
                showNotification();
                //Toast.makeText(this, "Je hebt een bericht ontvangen !!! ", Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
