package com.example.nzxt.chatapp2firebase;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class Profile extends AppCompatActivity {

    private final static int GALLERY_REQUEST = 100;
    private final static int CAMERA_REQUEST = 101;
    private Bitmap bitmap;
    private Uri filePath = null;
    private String rotate = "0";
    private EditText password1, password2;
    private Button updatePasswButton, updateImageButton;
    private String user, pass;
    private TextView goBack;
    private ImageView ivImage;
    private DatabaseReference refUsers;
    private FirebaseDatabase db;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private boolean blnPassw1 = false;
    private boolean blnPassw2 = false;
    private boolean blnImage = false;
    private boolean blnUpdatePassword = false;
    private boolean blnUpdateImage = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        password1 = (EditText) findViewById(R.id.passWord1);
        password2 = (EditText) findViewById(R.id.passWord2);
        updatePasswButton = (Button) findViewById(R.id.updatePasswButton);
        updateImageButton = (Button) findViewById(R.id.updateImageButton);
        ivImage = (ImageView) findViewById(R.id.imageView);
        goBack = (TextView) findViewById(R.id.tvGoBack);

        db = FirebaseDatabase.getInstance();
        refUsers = db.getReference("users");

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        //username.setText(UserDetails.username);
        //password.setText(UserDetails.passwordForUpdate);

        Picasso.with(Profile.this).load(UserDetails.imageUrlForUpdate).placeholder(R.drawable.person3).error(R.drawable.person3).rotate(Float.parseFloat(UserDetails.rotationForUpdate)).into(ivImage);

        registerForContextMenu(ivImage);

        updatePasswButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String passw1 = password1.getText().toString().trim();
                String passw2 = password2.getText().toString().trim();


                if (passw1.length() < 5) {
                    password1.setError("at least 5 characters long");
                } else if (passw2.length() < 5) {
                    password2.setError("at least 5 characters long");
                } else {
                    if (passw1.equals(passw2)) {
                        Toast.makeText(Profile.this, "Password succesfully updated!!!", Toast.LENGTH_LONG).show();
                        refUsers.child(UserDetails.username).child("password").setValue(password1.getText().toString());
                        password1.setText("");
                        password2.setText("");
                    } else {
                        Toast.makeText(Profile.this, "Password doesn't match !!!", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        updateImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(blnUpdateImage){
                    uploadImage();
                    blnUpdateImage = false;
                }else{
                    Toast.makeText(Profile.this, "Nothing to change !!!", Toast.LENGTH_LONG).show();
                }
            }
        });

        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void uploadImage() {
        final ProgressDialog mDialog = new ProgressDialog(this);
        mDialog.setMessage("Uploading...");
        mDialog.show();

        String imageName = UUID.randomUUID().toString();
        final StorageReference imageFolder = storageReference.child("images/" + imageName);
        if (filePath == null) {
            filePath = Uri.parse("android.resource://com.example.nzxt.chatapp2firebase/drawable/person");
        }
        imageFolder.putFile(filePath)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        mDialog.dismiss();
                        //Toast.makeText(Register.this, "Uploaded picture!!!", Toast.LENGTH_SHORT).show();
                        imageFolder.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {

                                // refUsers.child(UserDetails.username).child("name").setValue(username.getText().toString());
                                refUsers.child(UserDetails.username).child("rotation").setValue(rotate);
                                refUsers.child(UserDetails.username).child("url").setValue(uri.toString());
                                Toast.makeText(Profile.this, "Image successfully updated !!!", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        mDialog.dismiss();
                        Toast.makeText(Profile.this, "Er heeft zich een fout voor gedaan !!" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                // don't worry about this error
                double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                int i = (int) progress;
                mDialog.setMessage("Uploaded " + i + " %");
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Select Picture");
        menu.add(0, v.getId(), 0, "From Gallery");
        menu.add(0, v.getId(), 0, "From Camera");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle() == "From Gallery") {
            getImageFromGallery();
        } else if (item.getTitle() == "From Camera") {
            getImageFromCamera();
        } else {
            return false;
        }
        return true;
    }

    private void getImageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, GALLERY_REQUEST);
    }

    private void getImageFromCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, CAMERA_REQUEST);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                String pat = FilePath.getPath(Profile.this, filePath);
                rotate = getOrientation(pat);
                blnUpdateImage = true;

                Picasso.with(Profile.this).load(filePath).centerCrop().rotate(Float.valueOf(rotate)).resize(300, 300).into(ivImage);

            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                String pat = FilePath.getPath(Profile.this, filePath);
                rotate = getOrientation(pat);
                blnUpdateImage = true;

                Picasso.with(Profile.this).load(filePath).centerCrop().resize(300, 300).into(ivImage);

            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }


    private String getOrientation(String path) {
        rotate = "0";
        //String path = Environment.getExternalStorageDirectory() + "/DCIM/Camera/20170525_132621.jpg";
        //String path = pat;  // /sdcard/emulated/0/ ....
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //ExifInterface exif = new ExifInterface(Environment.getExternalStorageDirectory() + "/DCIM/Camera/20161116_120542.jpg");
        String rotationAmount = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        if (!TextUtils.isEmpty(rotationAmount)) {
            int rotationParam = Integer.parseInt(rotationAmount);
            switch (rotationParam) {
                case ExifInterface.ORIENTATION_NORMAL:
                    rotate = "0";
                    // Toast.makeText(Register.this, "rotate = " + rotate, Toast.LENGTH_LONG).show();
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = "90";
                    // Toast.makeText(Register.this, "rotate = " + rotate, Toast.LENGTH_LONG).show();
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = "180";
                    // Toast.makeText(Register.this, "rotate = " + rotate, Toast.LENGTH_LONG).show();
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = "270";
                    // Toast.makeText(Register.this, "rotate = " + rotate, Toast.LENGTH_LONG).show();
            }
        }
        return rotate;
    }
}
