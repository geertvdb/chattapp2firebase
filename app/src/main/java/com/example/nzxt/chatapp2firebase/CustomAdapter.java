package com.example.nzxt.chatapp2firebase;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by NZXT on 10-10-2017.
 */

public class CustomAdapter extends BaseAdapter {

    private Context context;
    private List<ChatMessage> chatMessages;

    public CustomAdapter(Context context, List<ChatMessage> chatMessages) {
        this.context = context;
        this.chatMessages = chatMessages;
    }

    @Override
    public int getCount() {
        if(chatMessages.size() != 0){
            return chatMessages.size();
        }
        return 0;

    }

    @Override
    public Object getItem(int position) {
        return chatMessages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;

        if(view == null){
            holder = new ViewHolder();
            view = LayoutInflater.from(context).inflate(R.layout.chat_item, viewGroup, false);
            holder.messageText = (TextView) view.findViewById(R.id.message_text);
            holder.messageUser = (TextView) view.findViewById(R.id.message_user);
            holder.messageTime = (TextView) view.findViewById(R.id.message_time);
            view.setTag(holder);
        }else{
            holder = (ViewHolder)view.getTag();
        }

        ChatMessage chatMessage = (ChatMessage)getItem(position);
        holder.messageText.setText(chatMessage.getMessageText());
        holder.messageTime.setText(chatMessage.getMessageTime());
        holder.messageUser.setText(chatMessage.getMessageName());

        if (chatMessage.getMessageName() != null && chatMessage.getMessageName() != "") {
            String name = chatMessage.getMessageName();
            if (name.equals(UserDetails.username)) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.weight = 1.0f;
                params.gravity = Gravity.RIGHT;
                //messageUser.setText(FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
                holder.messageText.setBackgroundResource(R.drawable.bubble_green);
                holder.messageTime.setLayoutParams(params);
                holder.messageText.setLayoutParams(params);
                holder.messageUser.setLayoutParams(params);
            } else {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.weight = 1.0f;
                params.gravity = Gravity.LEFT;
                holder.messageText.setBackgroundResource(R.drawable.bubble_yellow);
                // messageUser.setText(FirebaseAuth.getInstance()..getDisplayName());
                holder.messageTime.setLayoutParams(params);
                holder.messageText.setLayoutParams(params);
                holder.messageUser.setLayoutParams(params);
            }
        }

        return view;
    }

    class ViewHolder {
        TextView messageText;
        TextView messageUser;
        TextView messageTime;
    }
}
