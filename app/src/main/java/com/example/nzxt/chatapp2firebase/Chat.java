package com.example.nzxt.chatapp2firebase;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.messaging.FirebaseMessaging;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Chat extends AppCompatActivity {
    LinearLayout layout;
    RelativeLayout layout_2;
    ImageView sendButton;
    EditText messageArea;
    ScrollView scrollView;
    Firebase reference1, reference2;
    ListView listView;
    CustomAdapter adapter;
    List<ChatMessage> chatMessages = new ArrayList<ChatMessage>();

    FirebaseDatabase database;
    DatabaseReference myRef;
    DatabaseReference myRefUsers;
    String rotate = "0";
    Toolbar myToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("messages");
        myRefUsers = database.getReference("users");

        View v = getLayoutInflater().inflate(R.layout.toolbar, null);
        myToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(myToolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = this.getSupportActionBar();

        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setElevation(10);

        UserDetails.isInForGround = true;

        new DownloadImageTask((ImageView) myToolbar.findViewById(R.id.tlbImage))
                .execute(UserDetails.imageUrl);


        FirebaseMessaging.getInstance().subscribeToTopic("notifications");

        sendButton = (ImageView) findViewById(R.id.sendButton);
        messageArea = (EditText) findViewById(R.id.messageArea);
        listView = (ListView) findViewById(R.id.list_messages);

        Firebase.setAndroidContext(this);

        reference1 = new Firebase("https://gsonvolleydemo.firebaseio.com/messages/" + UserDetails.username + "_" + UserDetails.chatWith);
        reference2 = new Firebase("https://gsonvolleydemo.firebaseio.com/messages/" + UserDetails.chatWith + "_" + UserDetails.username);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageText = messageArea.getText().toString();
                UserDetails.isSender = true;

                if (!messageText.equals("")) {
                    ChatMessage chatMessage = new ChatMessage();
                    chatMessage.setMessageName(UserDetails.username);
                    chatMessage.setMessageText(messageText);
                    chatMessage.setMessageTime(DateFormat.format("dd-MM-yyyy (HH:mm:ss)", new Date().getTime()).toString());
                    reference1.push().setValue(chatMessage);
                    reference2.push().setValue(chatMessage);
                    messageArea.setText("");

                    myRefUsers.child(UserDetails.chatWith).child(UserDetails.username).setValue(true);
                    myRefUsers.child(UserDetails.chatWith).child("readMessage").setValue("false");
                    //  myRefUsers.child(UserDetails.username).child("showRedDot").setValue("true");
                }


            }
        });

        reference1.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                ChatMessage chatMessage = new ChatMessage();
                chatMessage = dataSnapshot.getValue(ChatMessage.class);

                if (chatMessage != null) {
                    chatMessages.add(chatMessage);

                    adapter = new CustomAdapter(getBaseContext(), chatMessages);
                    listView.setAdapter(adapter);
                    listView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
                    listView.setStackFromBottom(true);
                } else {
                    Toast.makeText(Chat.this, "Er ging iets verkeerd, probeer opnieuw !!", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            InputStream in = null;
            try {
                in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            } finally {
                try {
                    if(in != null){
                        in.close();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {

            TextView tv = (TextView) myToolbar.findViewById(R.id.tlbText);
            tv.setText(UserDetails.chatWith);
            Picasso.with(Chat.this).load(UserDetails.imageUrl).rotate(Float.parseFloat(UserDetails.rotation)).resize(300, 300).centerCrop().into(bmImage);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        UserDetails.isInForGround = true;
    }

    @Override
    protected void onStop() {

        UserDetails.isInForGround = false;
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        UserDetails.isInForGround = false;
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        myRefUsers.child(UserDetails.username).child(UserDetails.chatWith).setValue(false);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 300);

    }
}