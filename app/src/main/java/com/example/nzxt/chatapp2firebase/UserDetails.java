package com.example.nzxt.chatapp2firebase;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.List;

/**
 * Created by NZXT on 5-10-2017.
 */

public class UserDetails {
    static String username = "";
    static String password = "";
    static String chatWith = "";
    static String imageUrl = "";
    static String rotation = "";
    static String strTemp = "";
    static String imageUrlForUpdate;
    static String passwordForUpdate;
    static String rotationForUpdate;
    static int counter = 0;
    static boolean isSender = false;
    static boolean isInForGround = false;
    static boolean isOnline = false;
    static List<String> chatters;
    static NotificationManager notificationManager;
    static int serviceStartID;
    static Intent service;

    public static boolean isConnectedToInternet(Context context){
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if(connectivityManager != null){
            NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
            if(info != null){
                for(int i = 0; i < info.length; i++){
                    if(info[i].getState() == NetworkInfo.State.CONNECTED){
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
