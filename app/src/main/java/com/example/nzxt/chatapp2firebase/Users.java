package com.example.nzxt.chatapp2firebase;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import io.paperdb.Paper;

public class Users extends AppCompatActivity {
    private ListView usersList;
    private TextView noUsersText;
    private ArrayList<String> al = new ArrayList<>();
    private int totalUsers = 0;
    private ProgressDialog pd;
    private LinearLayout linearLayout;
    private List<String> lstUserNames;
    private List<ChatMessage> userList;

    // voor firebase notifications
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private DatabaseReference myRefUsers;
    private String dataTitle;
    private String dataMessage;

    private String imageURL;
    private SwipeRefreshLayout swipeRefreshLayout;
    private JSONObject obj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("messages");

        myRefUsers = database.getReference("users");

        UserDetails.isInForGround = true;

        Paper.init(Users.this);

        if(UserDetails.notificationManager.equals(null)&& UserDetails.notificationManager.equals("null")&& UserDetails.notificationManager.equals("")){
            UserDetails.notificationManager.cancelAll();
        }


        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe);

        usersList = (ListView) findViewById(R.id.usersList);
        noUsersText = (TextView) findViewById(R.id.noUsersText);
        linearLayout = (LinearLayout) findViewById(R.id.llUsers);

        pd = new ProgressDialog(Users.this);
        pd.setMessage("Loading...");
        pd.show();

        loadUsers();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Toast.makeText(Users.this, "Refresh", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(UserDetails.isConnectedToInternet(getBaseContext())){
                            loadUsers();
                        }else{
                            Toast.makeText(Users.this, "Check your internet connection!!", Toast.LENGTH_SHORT).show();
                        }

                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 1500);

            }
        });

        usersList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NotificationManager manager = (NotificationManager) getBaseContext().getSystemService(Context.NOTIFICATION_SERVICE);
                manager.cancelAll();
                UserDetails.chatWith = al.get(position);
                ChatMessage chatMessage = new ChatMessage();
                chatMessage = userList.get(position);
                imageURL = chatMessage.getImageURL();
                UserDetails.imageUrl = imageURL;
                UserDetails.rotation = chatMessage.getRotation();
                TextView txtView = (TextView) view.findViewById(R.id.tvUserName);
                String name = txtView.getText().toString();
                myRefUsers.child(UserDetails.username).child(name).setValue(false);

                if(UserDetails.isConnectedToInternet(getBaseContext())){
                    startActivity(new Intent(Users.this, Chat.class));
                }else{
                    Toast.makeText(Users.this, "Check your internet connection!!", Toast.LENGTH_SHORT).show();
                }


            }
        });

    }

    private void loadUsers() {

        //String url = "https://androidchatapp-76776.firebaseio.com/users.json";
        String url = "https://gsonvolleydemo.firebaseio.com/users.json";

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                doOnSuccess(s);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("" + volleyError);
            }
        });

        RequestQueue rQueue = Volley.newRequestQueue(Users.this);
        rQueue.add(request);
    }

    public void doOnSuccess(String s) {
        try {
            obj = new JSONObject(s);

            JSONObject name = new JSONObject(obj.toString());
            Log.d("JSONOBJECT", "json = " + name);
            Iterator i = obj.keys();
            String key = "";
            userList = new ArrayList<>();
            lstUserNames = new ArrayList<>();
            ChatMessage chatMessage;

            while (i.hasNext()) {

                key = i.next().toString();


                chatMessage = new ChatMessage();
                JSONObject objj = obj.getJSONObject(key);

                String naam = (String) objj.get("name");

                // for update user
                if(naam.equals(UserDetails.username)){
                    UserDetails.imageUrlForUpdate = (String)objj.get("url");
                    UserDetails.passwordForUpdate = (String)objj.get("password");
                    UserDetails.rotationForUpdate = (String)objj.get("rotation");
                }
                String url = (String) objj.get("url");
                String rotation = (String) objj.get("rotation");
                String isOnline = (String) objj.get("isOnline");
               // String showRedDot = (String) objj.get("showRedDot");
                chatMessage.setMessageUser(naam);
                chatMessage.setImageURL(url);
                chatMessage.setRotation(rotation);
                chatMessage.setOnline(isOnline);

                if (!chatMessage.getMessageUser().equals(UserDetails.username)) {
                    userList.add(chatMessage);
                    Log.d("Users.java", "naam = " + naam);
                }

                lstUserNames.add(naam);

                if (!key.equals(UserDetails.username)) {
                    al.add(key);
                }

                totalUsers++;
            }

            UserDetails.chatters = lstUserNames;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        UserAdapter adapter = new UserAdapter(Users.this, userList, obj);
        usersList.setAdapter(adapter);

        pd.dismiss();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_sign_out) {
            logOutDialog();
        }else if(item.getItemId() == R.id.menu_profiel){
            Intent intent = new Intent(Users.this, Profile.class);
            startActivity(intent);
        }
        return true;

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        pd.show();
        loadUsers();
        UserDetails.isInForGround = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        UserDetails.isInForGround = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        UserDetails.isInForGround = false;
    }

    private void logOutDialog(){
        android.app.AlertDialog dialog = new android.app.AlertDialog.Builder(Users.this)
                .setTitle("Log out")
                .setMessage("You want to log out???")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        myRefUsers.child(UserDetails.username).child("isOnline").setValue("false");
                        UserDetails.isOnline = false;
                        UserDetails.isInForGround = false;
                        if(UserDetails.service != null && !UserDetails.service.equals("")){
                            stopService(UserDetails.service);
                            UserDetails.username = null;
                        }


                        Paper.book().destroy();

                        finish();
                        Intent intent = new Intent(Users.this, MainActivity.class);
                        startActivity(intent);

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
