package com.example.nzxt.chatapp2firebase;

import android.*;
import android.Manifest;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;
import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import io.paperdb.Paper;

public class MainActivity extends AppCompatActivity {
    TextView registerUser;
    EditText username, password;
    Button loginButton;
    String user, pass;
    ChatMessage chatMessage = new ChatMessage();
    private String getCurrentDateTime;

    private FirebaseDatabase db;
    private DatabaseReference userList;
    private long time;

    private Context mContext;
    private LinearLayout mRelativeLayout;
    private PopupWindow mPopupWindow;

    public static final String TIME_SERVER = "time-b.nist.gov";

    String[] permissions = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE};

    public static final int MULTIPLE_PERMISSIONS = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //initialize
        registerUser = (TextView) findViewById(R.id.register);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        loginButton = (Button) findViewById(R.id.loginButton);
        mRelativeLayout = (LinearLayout) findViewById(R.id.rl);
        mContext = getApplicationContext();

        requestRuntimePermission();

        //Firebase
        db = FirebaseDatabase.getInstance();
        userList = db.getReference("users");

        //Paper
        Paper.init(MainActivity.this);

        //getintent if is one
        Intent intent = getIntent();
        if(intent != null){
            String name = intent.getStringExtra("name");
            String paswoord = intent.getStringExtra("passw");
            if(name != "" && name != null){
                if(paswoord != "" && paswoord != null){
                    username.setText(name);
                    password.setText(paswoord);
                }
            }
        }

        //getNetWorkTime
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                //final String result = performBlockingTask();
                try {
                    time = getNetworkTime();

                } catch (IOException e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //mTextView.setText(result);
                       // Toast.makeText(MainActivity.this, "NetworkTime = " + time, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
        thread.start();

        //Initialize NotificationManager
        UserDetails.notificationManager = (NotificationManager) getBaseContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if(compareDate("20/06/2017 20:00:00", time)){   //  MM/dd/yyyy HH:mm:ss
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);

            // Inflate the custom layout/view
            View customView = inflater.inflate(R.layout.custom_layout,null);

            // Initialize a new instance of popup window
            mPopupWindow = new PopupWindow(
                    customView,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );

            // Set an elevation value for popup window
            // Call requires API level 21
            if(Build.VERSION.SDK_INT>=21){
                mPopupWindow.setElevation(20.0f);
            }

            // Get a reference for the custom view close button
            ImageButton closeButton = (ImageButton) customView.findViewById(R.id.ib_close);

            // Set a click listener for the popup window close button
            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Dismiss the popup window
                    mPopupWindow.dismiss();
                    finish();
                }
            });

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Finally, show the popup window at the center location of root relative layout
                    mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER,0,0);
                    username.setText("");
                    password.setText("");
                    username.setEnabled(false);
                    password.setEnabled(false);
                }
            },1000);


        }else{
            // Remember user for login
            String paperUser = Paper.book().read("paperUser");
            if (paperUser != null) {
                if (!paperUser.equals("null") && !paperUser.equals("")) {
                    chatMessage.setOnline("true");
                    userList.child(paperUser).child("isOnline").setValue("true");
                    String passw = Paper.book().read("paperPassw");
                    if (passw != null && passw != "null" && !passw.equals("")) {
                        pass = passw;
                    }
                    user = paperUser;
                    loadUsers();
                }
            }
        }

        registerUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Register.class));
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(UserDetails.isConnectedToInternet(getBaseContext())){
                    user = username.getText().toString().trim();
                    pass = password.getText().toString().trim();

                    //register service
                    UserDetails.service = new Intent(MainActivity.this, ListenMessage.class);
                    startService(UserDetails.service);

                    if (user.equals("")) {
                        username.setError("can't be blank");
                    } else if (pass.equals("")) {
                        password.setError("can't be blank");
                    } else {

                        loadUsers();
                    }
                }else{
                    Toast.makeText(MainActivity.this, "Check your internet connection!!", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void loadUsers() {
        String url = "https://gsonvolleydemo.firebaseio.com/users.json";
        final ProgressDialog pd = new ProgressDialog(MainActivity.this);
        pd.setMessage("Loading...");
        pd.show();

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                if (s.equals("null")) {
                    Toast.makeText(MainActivity.this, "user not found", Toast.LENGTH_LONG).show();
                } else {
                    try {
                        JSONObject obj = new JSONObject(s);

                        if (!obj.has(user)) {
                            Toast.makeText(MainActivity.this, "user not found", Toast.LENGTH_LONG).show();
                        } else if (obj.getJSONObject(user).getString("password").equals(pass)) {
                            UserDetails.username = user;
                            UserDetails.password = pass;
                            UserDetails.isOnline = true;
                            Paper.book().write("paperUser", user);
                            Paper.book().write("paperPassw", pass);

                            chatMessage.setOnline("true");
                            userList.child(user).child("isOnline").setValue("true");
                            startActivity(new Intent(MainActivity.this, Users.class));
                            finish();
                        } else {
                            Toast.makeText(MainActivity.this, "incorrect password", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                pd.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("" + volleyError);
                pd.dismiss();
            }
        });

        RequestQueue rQueue = Volley.newRequestQueue(MainActivity.this);
        rQueue.add(request);
    }

    private boolean compareDate(String datetime, long time){
        // Get Current Date Time
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss aa");
        getCurrentDateTime = sdf.format(c.getTime());  //local time

        // long from internet
        //Date resultdata = new Date(time);
        //getCurrentDateTime = sdf.format(resultdata);

        Log.d("SYSTEMTIME" , "Time = " + getCurrentDateTime);
        //String getMyTime = "11/04/2017 11:28 PM ";
        String getMyTime = datetime;
        Log.d("getCurrentDateTime", getCurrentDateTime);
        // getCurrentDateTime: 05/23/2016 18:49 PM

        boolean compare = getCurrentDateTime.compareTo(getMyTime) < 0;
        if(compare)
        {
           // Toast.makeText(MainActivity.this, "de app werkt nog ", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            Log.d("Return", "getMyTime older than getCurrentDateTime ");

            //Toast.makeText(MainActivity.this, "De tijd is verstreken de app werkt niet meer ", Toast.LENGTH_SHORT).show();
            return true;
        }
    }

    public static long getNetworkTime() throws IOException {
        NTPUDPClient timeClient = new NTPUDPClient();
        InetAddress inetAddress = InetAddress.getByName(TIME_SERVER);
        TimeInfo timeInfo = timeClient.getTime(inetAddress);
        return timeInfo.getMessage().getReceiveTimeStamp().getTime();
    }

    private void requestRuntimePermission() {

        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE) + ContextCompat
                .checkSelfPermission(this, android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(
                            this, android.Manifest.permission.CAMERA)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA},
                        MULTIPLE_PERMISSIONS);

            }
        }
    }

    //RESULT permission granted or denied
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case MULTIPLE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(MainActivity.this, "permission granted success", Toast.LENGTH_LONG).show();

                } else {

                    Toast.makeText(MainActivity.this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }

    }
}
