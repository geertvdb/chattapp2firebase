package com.example.nzxt.chatapp2firebase;

/**
 * Created by NZXT on 11-10-2017.
 */

public class Message {


    // for notifications later
    private String title, message;

    public Message(){}

    public Message(String title, String message) {
        this.title = title;
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

}
