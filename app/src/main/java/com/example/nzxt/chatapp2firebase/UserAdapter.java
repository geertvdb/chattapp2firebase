package com.example.nzxt.chatapp2firebase;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.ExifInterface;
import android.media.Image;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

/**
 * Created by NZXT on 14-10-2017.
 */

public class UserAdapter extends BaseAdapter {

    private Context context;
    private List<ChatMessage> users;
    private String rotate;
    private ChatMessage chatMessage;
    private String strJsonResponse;
    private boolean redDot;
    private JSONObject obj;

    public UserAdapter(Context context, List<ChatMessage> chatMessages, JSONObject obj) {
        this.context = context;
        this.users = chatMessages;
        this.obj = obj;
    }

    @Override
    public int getCount() {
        if (users.size() != 0) {
            return users.size();
        }
        return 0;

    }

    @Override
    public Object getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        UserAdapter.ViewHolder holder;

        if (view == null) {
            holder = new UserAdapter.ViewHolder();
            view = LayoutInflater.from(context).inflate(R.layout.user_item, viewGroup, false);
            holder.user = (TextView) view.findViewById(R.id.tvUserName);
            holder.image = (ImageView) view.findViewById(R.id.ivUserImage);
            holder.onlineDot = (ImageView) view.findViewById(R.id.ivOnlineDot);
            holder.redDot = (ImageView) view.findViewById(R.id.message_item_count);

            view.setTag(holder);
        } else {
            holder = (UserAdapter.ViewHolder) view.getTag();
        }

        chatMessage = (ChatMessage) getItem(position);

        String isOnline = "false";
        String rotation = chatMessage.getRotation();
        isOnline = chatMessage.isOnline();
        //String naam = chatMessage.getMessageUser().toLowerCase();
        String naam = chatMessage.getMessageUser();
        holder.user.setText(naam);

        if (isOnline.equals("true")) {
             //holder.onlineDot.setImageResource(R.drawable.greendot);
            holder.onlineDot.setBackgroundColor(Color.GREEN);
           // Log.d("Agreendot", "gepasseerd " + chatMessage.getMessageUser() + " " + isOnline);
        } else {
            holder.onlineDot.setBackgroundColor(Color.LTGRAY);
        }
        Picasso.with(context).load(chatMessage.getImageURL()).rotate(Float.parseFloat(rotation)).resize(300, 300).centerCrop().into(holder.image);

        try {
            //JSONObject obj = new JSONObject(strJsonResponse);
            JSONObject objj = obj.getJSONObject(UserDetails.username);
            redDot = (boolean) objj.get(naam);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(redDot == true){
            holder.redDot.setImageResource(R.drawable.reddot);
        }else{
            holder.redDot.setImageResource(R.drawable.silverdot);
        }

        return view;
    }

    class ViewHolder {
        TextView user;
        ImageView image;
        ImageView onlineDot;
        ImageView redDot;

    }

  /*  TextDrawable drawable = TextDrawable.builder()
            .buildRound("" + listData.get(position).getQuantity(), Color.RED);
        holder.img_cart_count.setImageDrawable(drawable); */


}
