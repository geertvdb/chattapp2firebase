package com.example.nzxt.chatapp2firebase;

import android.*;
import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
//import com.google.firebase.F

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.MalformedInputException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class Register extends AppCompatActivity {
    EditText username, password, password2;
    Button registerButton;
    String user, pass, pass2;
    TextView login;
    ImageView ivImage;
    private final static int GALLERY_REQUEST = 100;
    private final static int CAMERA_REQUEST = 101;
    private Bitmap bitmap;
    private Uri filePath = null;
    private String rotate = "0";
    private DatabaseReference refUsers;
    private FirebaseDatabase db;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private ChatMessage chatMessage;
    private List<String> lstUserNames;
    private List<ChatMessage> userList;
    private ProgressDialog pd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        if(UserDetails.isConnectedToInternet(getBaseContext())){
            storage = FirebaseStorage.getInstance();
            storageReference = storage.getReference();

            db = FirebaseDatabase.getInstance();
            refUsers = db.getReference("users");

            pd = new ProgressDialog(Register.this);
            pd.setMessage("Loading...");
            pd.show();

            loadUsers();


            username = (EditText) findViewById(R.id.username);
            password = (EditText) findViewById(R.id.password);
            password2 = (EditText) findViewById(R.id.password2);
            registerButton = (Button) findViewById(R.id.registerButton);
            login = (TextView) findViewById(R.id.login);
            ivImage = (ImageView) findViewById(R.id.imageView);

            registerForContextMenu(ivImage);

            Firebase.setAndroidContext(this);

            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Register.this, MainActivity.class);
                    intent.putExtra("name", user);
                    intent.putExtra("passw", pass);
                    startActivity(intent);
                    finish();
                }
            });

            registerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    user = username.getText().toString().trim();
                    pass = password.getText().toString().trim();
                    pass2 = password2.getText().toString().trim();

                    if (user.equals("")) {
                        username.setError("can't be blank");
                    } else if (pass.equals("")) {
                        password.setError("can't be blank");
                    } else if (!user.matches("[A-Za-z0-9]+")) {
                        username.setError("only alphabet or number allowed");
                    } else if (user.length() < 5) {
                        username.setError("at least 5 characters long");
                    } else if (pass.length() < 5) {
                        password.setError("at least 5 characters long");
                    } else if (pass2.length() < 5){
                        password2.setError("at least 5 characters long");
                    }else{
                        if(pass.equals(pass2)){
                            uploadImage();
                        }else{
                            Toast.makeText(Register.this, "Password doesn't match !!!", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            });
        }else{
            Toast.makeText(Register.this, "Check your internet connection!!", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Register.this,MainActivity.class);
            startActivity(intent);
        }

    }

    private void registerUser() {
        //String url = "https://androidchatapp-76776.firebaseio.com/users.json";
        String url = "https://gsonvolleydemo.firebaseio.com/users.json";

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                //Firebase reference = new Firebase("https://androidchatapp-76776.firebaseio.com/users");
                Firebase reference = new Firebase("https://gsonvolleydemo.firebaseio.com/users");


                if (s.equals("null")) {

                    String pass = chatMessage.getPassword();
                    String name = chatMessage.getMessageUser();
                    String url = chatMessage.getImageURL();
                    String rotation = chatMessage.getRotation();
                    String isOnline = chatMessage.isOnline();
                    String readMessage = chatMessage.isReadMessage();


                    // reference.child(user).setValue(chatMessage);
                    reference.child(user).child("password").setValue(pass);
                    reference.child(user).child("name").setValue(name);
                    reference.child(user).child("url").setValue(url);
                    reference.child(user).child("rotation").setValue(rotation);
                    reference.child(user).child("isOnline").setValue(isOnline);
                    reference.child(user).child("readMessage").setValue(readMessage);

                    for(int i = 0; i < lstUserNames.size(); i++){
                        reference.child(user).child(lstUserNames.get(i)).setValue(false);
                        reference.child(lstUserNames.get(i)).child(user).setValue(false);
                    }
                    lstUserNames.add(user);
                    UserDetails.chatters = lstUserNames;

                    Toast.makeText(Register.this, "Registration successfull", Toast.LENGTH_LONG).show();
                } else {
                    try {
                        JSONObject obj = new JSONObject(s);

                        if (!obj.has(user)) {
                            //  ChatMessage chatMessage = new ChatMessage();

                            String pass = chatMessage.getPassword();
                            String name = chatMessage.getMessageUser();
                            String url = chatMessage.getImageURL();
                            String rotation = chatMessage.getRotation();
                            String isOnline = chatMessage.isOnline();
                            String readMessage = chatMessage.isReadMessage();

                            // reference.child(user).setValue(chatMessage);
                            reference.child(user).child("password").setValue(pass);
                            reference.child(user).child("name").setValue(name);
                            reference.child(user).child("url").setValue(url);
                            reference.child(user).child("rotation").setValue(rotation);
                            reference.child(user).child("isOnline").setValue(isOnline);
                            reference.child(user).child("readMessage").setValue(readMessage);

                            for(int i = 0; i < lstUserNames.size(); i++){
                                reference.child(user).child(lstUserNames.get(i)).setValue(false);
                                reference.child(lstUserNames.get(i)).child(user).setValue(false);
                            }

                            lstUserNames.add(user);
                            UserDetails.chatters = lstUserNames;

                            Toast.makeText(Register.this, "Registration successfull", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(Register.this, "Username already exists", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

                // pd.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("" + volleyError);
                //  pd.dismiss();
            }
        });

        RequestQueue rQueue = Volley.newRequestQueue(Register.this);
        rQueue.add(request);
    }

    private void uploadImage() {
        final ProgressDialog mDialog = new ProgressDialog(this);
        mDialog.setMessage("Uploading...");
        mDialog.show();

        String imageName = UUID.randomUUID().toString();
        final StorageReference imageFolder = storageReference.child("images/" + imageName);
        if(filePath == null){
            filePath = Uri.parse("android.resource://com.example.nzxt.chatapp2firebase/drawable/person");
        }
        imageFolder.putFile(filePath)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        mDialog.dismiss();
                        //Toast.makeText(Register.this, "Uploaded picture!!!", Toast.LENGTH_SHORT).show();
                        imageFolder.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                chatMessage = new ChatMessage();
                                chatMessage.setMessageUser(user);
                                chatMessage.setPassword(pass);
                                chatMessage.setRotation(rotate);
                                chatMessage.setOnline("false");
                                chatMessage.setImageURL(uri.toString());
                                chatMessage.setReadMessage("true");

                                registerUser();

                                // old code
                                //set value for new Category if image upload and we can get downloadlink
                              /*  newFood = new Food();
                                newFood.setName(edtName.getText().toString());
                                newFood.setDescription(edtDescription.getText().toString());
                                newFood.setPrice(edtPrice.getText().toString());
                                newFood.setDiscount(edtDiscount.getText().toString());
                                newFood.setMenuId(categorieId);
                                newFood.setImage(uri.toString()); */
                                // Snackbar.make(drawer, "new category " + newCategory.getName() + " was added", Snackbar.LENGTH_SHORT).show();
                            }
                        });
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        mDialog.dismiss();
                        Toast.makeText(Register.this, "Het werkt niet" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                // don't worry about this error
                double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                int i = (int) progress;
                mDialog.setMessage("Uploaded " + i + " %");
            }
        });
    }

    private void loadUsers() {

        //String url = "https://androidchatapp-76776.firebaseio.com/users.json";
        String url = "https://gsonvolleydemo.firebaseio.com/users.json";

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                doOnSuccess(s);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("" + volleyError);
            }
        });

        RequestQueue rQueue = Volley.newRequestQueue(Register.this);
        rQueue.add(request);
    }

    public void doOnSuccess(String s) {
        try {
            JSONObject obj = new JSONObject(s);

            JSONObject name = new JSONObject(obj.toString());

            Iterator i = obj.keys();
            String key = "";
            userList = new ArrayList<>();
            lstUserNames = new ArrayList<>();
            ChatMessage chatMessage;

            while (i.hasNext()) {
                key = i.next().toString();

                chatMessage = new ChatMessage();
                JSONObject objj = obj.getJSONObject(key);
                String naam = (String) objj.get("name");

                chatMessage.setMessageUser(naam);

                lstUserNames.add(naam);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        pd.dismiss();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Select Picture");
        menu.add(0, v.getId(), 0, "From Gallery");
        menu.add(0, v.getId(), 0, "From Camera");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle() == "From Gallery") {
            getImageFromGallery();
        } else if (item.getTitle() == "From Camera") {
            getImageFromCamera();
        } else {
            return false;
        }
        return true;
    }

    private void getImageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, GALLERY_REQUEST);
    }

    private void getImageFromCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, CAMERA_REQUEST);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                String pat = FilePath.getPath(Register.this, filePath);
                rotate = getOrientation(pat);

                Picasso.with(Register.this).load(filePath).centerCrop().rotate(Float.valueOf(rotate)).resize(300, 300).into(ivImage);

            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                String pat = FilePath.getPath(Register.this, filePath);
                rotate = getOrientation(pat);

                Picasso.with(Register.this).load(filePath).centerCrop().resize(300, 300).into(ivImage);

            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }



    private String getOrientation(String path) {
        rotate = "0";
        //String path = Environment.getExternalStorageDirectory() + "/DCIM/Camera/20170525_132621.jpg";
        //String path = pat;  // /sdcard/emulated/0/ ....
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //ExifInterface exif = new ExifInterface(Environment.getExternalStorageDirectory() + "/DCIM/Camera/20161116_120542.jpg");
        String rotationAmount = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        if (!TextUtils.isEmpty(rotationAmount)) {
            int rotationParam = Integer.parseInt(rotationAmount);
            switch (rotationParam) {
                case ExifInterface.ORIENTATION_NORMAL:
                    rotate = "0";
                   // Toast.makeText(Register.this, "rotate = " + rotate, Toast.LENGTH_LONG).show();
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = "90";
                   // Toast.makeText(Register.this, "rotate = " + rotate, Toast.LENGTH_LONG).show();
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = "180";
                   // Toast.makeText(Register.this, "rotate = " + rotate, Toast.LENGTH_LONG).show();
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = "270";
                   // Toast.makeText(Register.this, "rotate = " + rotate, Toast.LENGTH_LONG).show();
            }
        }
        return rotate;
    }
}

